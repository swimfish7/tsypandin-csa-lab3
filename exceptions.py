#!/usr/bin/python3
# pylint: disable=missing-class-docstring
# pylint: disable=missing-module-docstring

class TranslationException(RuntimeError):
    pass


class NotAllowedSymbolException(TranslationException):
    pass


class VariableOrLabelDuplicationException(TranslationException):
    pass


class DataDeclarationException(TranslationException):
    pass


class CodeLineErrorException(TranslationException):
    pass


class LabelDeclarationException(TranslationException):
    pass


class UnknownOpcodeException(TranslationException):
    pass


class CodeRuntimeException(RuntimeError):
    pass


class UnexpectedOpcodeException(CodeRuntimeException):
    pass
