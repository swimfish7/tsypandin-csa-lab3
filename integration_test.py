#!/usr/bin/python3
# pylint: disable=missing-class-docstring
# pylint: disable=missing-module-docstring

import unittest

import machine
import translator


class IntegrationTest(unittest.TestCase):
    input = 'tests/input.txt'

    def translate_and_start(self, code, output):
        translator.main([code, output])
        return machine.main([output, self.input, 2048])

    def test_hello(self):
        output = self.translate_and_start('tests/hello', 'tests/hello_res')
        self.assertEqual(output, 'Hello, World')

    def test_cat(self):
        output = self.translate_and_start('tests/cat', 'tests/cat_res')
        self.assertEqual(output, 'SHOULD CAT THIS!')

    def test_prob1(self):
        output = self.translate_and_start('tests/prob1', 'tests/prob1_res')
        self.assertEqual(output, '233168')
