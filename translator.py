#!/usr/bin/python3
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=invalid-name                # сохраним традиционные наименования сигналов
# pylint: disable=consider-using-f-string     # избыточный синтаксис
# pylint: disable=missing-class-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=too-many-locals
import sys

import exceptions
from isa import Opcode, AddrMode, write_code, Term

str_to_opcode = {
    'data': Opcode.DATA,

    'lw': Opcode.LW,
    'sw': Opcode.SW,
    'add': Opcode.ADD,

    'jmp': Opcode.JMP,
    'je': Opcode.JE,
    'jge': Opcode.JGE,

    'hlt': Opcode.HLT
}

characters = set('abcdefghijklmnopqrstuvwxyz')
numbers = set('0123456789')
symbols = set('.:-\'')
allowed_symbols = characters | numbers | symbols

required_labels = {'.data', '.text', '.start'}


def translate(text):
    memory_cell_cnt = 0
    terms = []
    labels = {}
    variables = {}
    skip_translator_list = []

    is_data_declaration = True
    # Start program from .start label
    terms.append(Term(memory_cell_cnt, 'jmp', AddrMode.ABS, '.start'))
    memory_cell_cnt += 1

    # Memory mapped IO, cells as variables for input and output
    # Also, we will add these cells to translator skip lines list
    terms.append(Term(memory_cell_cnt, 'data', AddrMode.DATA, 0))
    skip_translator_list.append(memory_cell_cnt)
    memory_cell_cnt += 1

    terms.append(Term(memory_cell_cnt, 'data', AddrMode.DATA, 0))
    skip_translator_list.append(memory_cell_cnt)
    memory_cell_cnt += 1

    def validate_label_or_var(label_or_var: str):
        if label_or_var in labels.keys() | variables.keys():
            raise exceptions.VariableOrLabelDuplicationException(
                'Line or variable ' + label_or_var + ' is already exists')

    for line in text.split('\n'):
        line_words = [word.strip() for word in line.split()]

        # Validate line code
        if len(line_words) == 0:
            continue

        for word in line_words:
            for word_chr in word:
                if word_chr not in allowed_symbols:
                    exception_msg = 'Char ' + word_chr + ' is not allowed'
                    raise exceptions.NotAllowedSymbolException(exception_msg)

        if len(line_words) >= 3:
            exception_msg = 'Unknown construction: too much words in line of code'
            raise exceptions.TranslationException(exception_msg)

        # Process labels
        if len(line_words) == 1 and line_words[0][0] == '.' and line_words[0][-1] == ':':
            label = line_words[0][0:-1]
            validate_label_or_var(label)
            labels[label] = memory_cell_cnt

            # If we got .data label, then we are processing data lines now
            is_data_declaration = bool(label == '.data')
            continue

        # Process data line or code line
        if is_data_declaration:
            if len(line_words) != 2:
                exception_msg = 'Data declaration should contain exactly 2 words'
                raise exceptions.DataDeclarationException(exception_msg)
            var_name = line_words[0]
            validate_label_or_var(var_name)
            variables[var_name] = memory_cell_cnt

            var_value = line_words[1]
            if len(var_value) == 3 and var_value[0] == '\'' and var_value[2] == '\'':
                terms.append(Term(memory_cell_cnt, 'data', AddrMode.DATA, ord(var_value[1])))
            else:
                for word_chr in var_value:
                    if word_chr not in numbers | {'-'}:
                        raise exceptions.DataDeclarationException(
                            'Data declaration should contain only digits or backticks')
                terms.append(Term(memory_cell_cnt, 'data', AddrMode.DATA, var_value))
        else:
            if len(line_words) > 2:
                exception_msg = 'Code line should contain 1 or 2 words'
                raise exceptions.CodeLineErrorException(exception_msg)

            addr_mode = AddrMode.DATA
            if line_words[0] in {'in', 'out'}:
                if line_words[0] == 'in':
                    line_words[0] = 'lw'
                    line_words.append(1)
                else:
                    line_words[0] = 'sw'
                    line_words.append(2)

            if len(line_words) == 1:
                # dummy zero argument
                line_words.append(0)

            op_code = line_words[0]
            if op_code not in str_to_opcode.keys():
                raise exceptions.UnknownOpcodeException('Unknown opcode ' + op_code)
            terms.append(Term(memory_cell_cnt, line_words[0], addr_mode, line_words[1]))

        memory_cell_cnt += 1

    for label in required_labels:
        if label not in labels.keys():
            raise exceptions.LabelDeclarationException('No required ' + label + ' label found')

    code = []

    # Link labels and variables and write it into JSON structure
    for term in terms:
        if term in skip_translator_list:
            continue

        if term.argument in labels.keys():
            term = Term(term.line, term.operation, AddrMode.ABS, labels[term.argument])
        elif term.argument in variables.keys():
            term = Term(term.line, term.operation, AddrMode.ABS, int(variables[term.argument]))
        else:
            term = Term(term.line, term.operation, AddrMode.DATA, term.argument)

        code.append({'opcode': str_to_opcode[term.operation], 'term': term})

    return code


def main(args):
    try:
        source_fp, target_fp = args

        with open(source_fp, 'rt', encoding='utf-8') as f:
            source_fp = f.read()

        code = translate(source_fp)
        print('source LoC:', len(source_fp.split()), 'code instr:', len(code))
        write_code(target_fp, code)
    except exceptions.TranslationException as e:
        print(e)


if __name__ == '__main__':
    msg = 'Wrong number of arguments.\nUsage: translator.py <input_code_file> <target_file>'
    assert len(sys.argv) == 3, msg
    main(sys.argv[1:])
