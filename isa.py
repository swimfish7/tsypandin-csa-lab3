#!/usr/bin/python3
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=invalid-name                # сохраним традиционные наименования сигналов
# pylint: disable=consider-using-f-string     # избыточный синтаксис
# pylint: disable=missing-class-docstring
# pylint: disable=missing-module-docstring

import json
from collections import namedtuple
from enum import Enum


class Opcode(str, Enum):
    DATA = 'data'

    LW = 'lw'
    SW = 'sw'
    ADD = 'add'

    JMP = 'jmp'
    JE = 'je'
    JGE = 'jge'

    HLT = 'hlt'


class AddrMode(str, Enum):
    ABS = 'absolute'
    DATA = 'data'


class Term(namedtuple('Term', 'line operation mode argument')):
    pass


def write_code(filename, code):
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))


def read_code(filename):
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())

    for instr in code:
        instr['opcode'] = Opcode(instr['opcode'])
        if 'term' in instr:
            instr['term'] = Term(instr['term'][0], instr['term'][1],
                                 AddrMode(instr['term'][2]), instr['term'][3])

    return code
