# Лабораторная работа #3

Цыпандин Николай P33111

Вариант: `asm | acc | neum | hw | tick | struct | stream | mem | prob1`

## Структура проекта

Исполняемые модули:

- `translator.py` - Транслятор, транслирует пользовательский код в машинный, принимает на вход:
    - файл с кодом
    - файл, куда записать результат
- `machine.py` - Машина, исполняющая код, принимает на вход:
    - файл с транслированным кодом
    - файл с входными данными
    - размер памяти

Прочее:

- `isa.py` - instruction set architecture, описывает опкоды и архитектуру команд
- `exceptions.py` - Содержит описание ошибок

## Язык программирования

EBNF

```
<letter> ::= "a" | "b"
       | "c" | "d" | "e" | "f" | "g" | "h" | "i"
       | "j" | "k" | "l" | "m" | "n" | "o" | "p"
       | "q" | "r" | "s" | "t" | "u" | "v" | "w"
       | "x" | "y" | "z"
<name> ::= <letter> <name> | <letter>
<label> ::= "." <name>

<digit> ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" 
<number> ::= <digit> <number> | <digit>

<empty_expr> ::= "\n"

<op_0_arg> ::= "hlt" | "in" | "out"
<op_1_arg> ::= "lw" | "sw" | "add" | 
<op_jmp> ::= "jmp" | "je" | "jge" label
<label_def> ::= label | "section .text" | "section .data" | ".start" ":"

<line> ::= <label_def> | <op_0_arg> | <op_1_arg> " " <name> |  <op_jmp> " " <label> | <name> " " <number> | <empty_expr>

<program> ::= <line> <program> | <line>
```

- Комментарии не поддерживаются
- Поддерживаются метки: если в строке ровно 1 слово, оно начинается с "." и заканчивается ":" -
  это считается меткой. На метки можно перемещаться с помощью команд типа <i>J</i>.

- Обязательные метки:
    - `.data` - специальная метка, сигнализирующая о начале области данных. Может содержать только объявление
      переменных.
      Писать код непосредственно после неё нельзя
    - `.text` - специальная метка, содержащая исходный код программы, все инструкции пишутся здесь
    - `.start` - Entrypoint, сигнализирует транслятору о том, что с этого места начинается исполнение программы
    - Пример:

      ```
      .data:
          temp 1000
      .text:
          .start:
            lw temp
            add 7
            sw temp
            jmp .start
      ```


- Поддерживаются переменные: любая строка в области данных должна состоять из 2 слов - имени переменной и значения.
  Значением переменной может быть только число
- Никакая метка или переменная не может пересекаться по названиям:
    - Пример:
        ```
        .data:
            temp 42
            other_temp 'Hello, world!' <- wrong
            pointer temp <- wrong
        .text:
            .start:
                ...
            .temp: <- wrong
        ```

- В секции .text пишутся команды, одна на каждую строку
- Код выполняется последовательно
- Есть синтаксический сахар, в виде команд `in`, `out` (MMIO):
    - `in` – будет транслировано в инструкцию lw с автоматической переменной
    - `out` – будет транслировано в инструкцию sw с автоматической переменной

## Система команд

Особенности процессора:

- Машинное слово – 64 бит, знаковое.
- Регистры:
    - `acc` - регистр, вокруг которого построена система команд
    - `ip` - регистр, адресующий в памяти текущую исполняющуюся инструкцию. Увеличивается на 1 после каждой инструкции.
      Можно менять значение регистра программно, с помощью команд перехода
    - `addr` - адресный регистр, адресующий данные в памяти
    - `step_counter` - Считает количество тактов в процессе исполнения инструкции
    - `z_flag, n_flag` - флаговые регистры
        - Z - выставляется автоматически, когда после предыдущей операции в аккумуляторе оказался 0
        - N - выставляется автоматически, когда в аккумуляторе лежит отрицательное число (старший бит равен 1)


- Ввод-вывод – мапится на память, адреса которых зашиты. запись/чтение из них
  отлавливаются и перенаправляются в соответствующие буферы
- Поддерживаемые режимы адресации:
    - ABS - абсолютная адресация, аргумент инструкции - адрес, по которому лежит операнд
    - DATA - без адресации, аргумент инструкции - операнд

  Режимы адресации поддерживаются неявно, на практике пользователь может работать только с переменными

### Набор инструкций

- В коде в качестве аргумента инструкции могут быть использованы только переменные
- После трансляции:
    - Число - остается числом
    - Переменная - вместо нее подставляется непосредственно адрес в памяти, по которой она расположена
    - Метка - вместо нее подставляется адрес ячейки памяти, идущей за меткой

| Syntax         | Mnemonic       | Кол-во тактов | Comment                                                        |
|:---------------|:---------------|---------------|:---------------------------------------------------------------|
| `lw [temp]`    | lw `[temp]`    | 2             | загрузить значение переменной в аккумулятор                    |
| `sw [temp]`    | sw `[temp]`    | 2             | сохранить значение аккумулятора в переменную                   |
| `add [temp]`   | add `[temp]`   | 2             | прибавить значение из памяти к аккумулятору                    |
| `jmp [.label]` | jmp `[.label]` | 1             | безусловный переход к инструкции сразу после метки             |
| `je [.label]`  | je `[.label]`  | 1             | переход к инструкции сразу после метки, если установлен флаг Z |
| `jge [.label]` | jge `[.label]` | 1             | переход к инструкции сразу после метки, если установлен флаг N |
| `hlt`          | hlt            | 0             | останов                                                        |

### Кодирование инструкций

- Машинный код сериализуется в виде массива в структуру JSON
- Один элемент списка, одна инструкция
- Индекс списка – адрес инструкции. Может быть использовано командой перехода

Пример:

```json
[
  ...
  {
    "opcode": "lw",
    "term": [
      12,
      "lw",
      "absolute",
      3
    ]
  },
  {
    "opcode": "out",
    "term": [
      13,
      "out",
      "data",
      0
    ]
  },
  ...
]
```

где:

- `opcode` – код операции
- `term` – полная информация об инструкции (номер, код, режим аддресации, аргумент)

Типы данные в модуле [isa](./isa.py), где:

- `Opcode` – enum кодов операций;
- `AddrMode` – enum режимов адресации;
- `Term` – структура для описания всей необходимой информации об инструкции.

## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file>`

Реализовано в модуле: [translator](./translator.py)

Этапы трансляции (функция `translate`):

1. Трансформирование текста программы в последовательность термов
    - Валидирует исходный код 'белым' списком возможных символов, опкодов
2. Проверка наличия обязательных меток
3. Генерация машинного кода

Правила генерации машинного кода:

- один терм – одна инструкция;
- вместо переменных подставляются их адреса, вместо меток - адреса следующих ячеек в памяти

## Модель процессора

Реализовано в модуле: [machine](./machine.py).

DataPath и ControlUnit объединены в один структурный элемент для наглядности и удобства
одного структурного элемента.

![](./model.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `latch_acc` – защёлкнуть аккумулятор. Принимает управляющий сигнал, указывающий на источник данных. Автоматически
  защелкивает Z и P флаги. Если источником указан АЛУ, необходимо так же передать 3 дополнительных управляющих сигнала:
  один отвечает за код операции, один - за источник данных для второго операнда, и еще один за то, стоит ли сохранить
  результат вычислений, или заменить его текущим значением аккумулятора;
- `latch_addr` – защёлкнуть аддресный регистр. Принимает управляющий сигнал, указывающий на источник данных;
- `latch_step_counter` – защелкнуть счетчик шагов исполнения инструкции. Принимает управляющий сигнал, указывающий на
  источник данных;
- `latch_ip` – защелкнуть instruction pointer. Принимает управляющий сигнал, указывающий на источник данных;
- `latch_mem` – выгрузить данные из аккумулятора в память;

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль `logging`
- Количество инструкций для моделирования ограничено hardcoded константой
- Остановка моделирования осуществляется при помощи исключения StopIteration
- Управление симуляцией реализовано в функции `start_simulation`

## Апробация

* Тесты поделены на 2 группы:
    * [транслятора](translator_test.py)
    * [симуляции в целом](integration_test.py)

1. [hello world](./examples/hello) – вывод строки `'Hello, World'`
2. [cat](./examples/cat) – программа `cat`, повторяющая ввод на выводе, по сути echo
3. [prob1](./examples/prob1) – 1-ая проблема из сайта [projecteuler](https://projecteuler.net)

Пример для `cat`:

```
cat examples/hello
.data:
.text:
	.start:
		in
		je .end
		out
		jmp .start
	.end:
		hlt

python translator.py examples/cat examples/cat_res
source LoC: 11 code instr: 8

cat examples/cat_res
[
    {
        "opcode": "jmp",
        "term": [
            0,
            "jmp",
            "absolute",
            3
        ]
    },
    {
        "opcode": "data",
        "term": [
            1,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            2,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "lw",
        "term": [
            3,
            "lw",
            "data",
            1
        ]
    },
    {
        "opcode": "je",
        "term": [
            4,
            "je",
            "absolute",
            7
        ]
    },
    {
        "opcode": "sw",
        "term": [
            5,
            "sw",
            "data",
            2
        ]
    },
    {
        "opcode": "jmp",
        "term": [
            6,
            "jmp",
            "absolute",
            3
        ]
    },
    {
        "opcode": "hlt",
        "term": [
            7,
            "hlt",
            "data",
            0
        ]
    }
]

cat examples/input.txt
SHOULD CAT THIS!

python machine.py examples/cat_res examples/input.txt 2048
Output:
SHOULD CAT THIS!
instr_counter: 67 ticks: 101
```

Журнал выполнения программы cat:

```
DEBUG:root:{TICK: 0, IP: 0, ADDR: 0, ACC: 0, Z: 0, N: 0, SC: 0} 0 jmp absolute 3
DEBUG:root:{TICK: 1, IP: 3, ADDR: 0, ACC: 0, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 2, IP: 3, ADDR: 1, ACC: 0, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 3, IP: 4, ADDR: 1, ACC: 83, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 4, IP: 5, ADDR: 1, ACC: 83, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 5, IP: 5, ADDR: 2, ACC: 83, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 6, IP: 6, ADDR: 2, ACC: 83, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 7, IP: 3, ADDR: 2, ACC: 83, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 8, IP: 3, ADDR: 1, ACC: 83, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 9, IP: 4, ADDR: 1, ACC: 72, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 10, IP: 5, ADDR: 1, ACC: 72, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 11, IP: 5, ADDR: 2, ACC: 72, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 12, IP: 6, ADDR: 2, ACC: 72, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 13, IP: 3, ADDR: 2, ACC: 72, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 14, IP: 3, ADDR: 1, ACC: 72, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 15, IP: 4, ADDR: 1, ACC: 79, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 16, IP: 5, ADDR: 1, ACC: 79, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 17, IP: 5, ADDR: 2, ACC: 79, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 18, IP: 6, ADDR: 2, ACC: 79, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 19, IP: 3, ADDR: 2, ACC: 79, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 20, IP: 3, ADDR: 1, ACC: 79, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 21, IP: 4, ADDR: 1, ACC: 85, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 22, IP: 5, ADDR: 1, ACC: 85, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 23, IP: 5, ADDR: 2, ACC: 85, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 24, IP: 6, ADDR: 2, ACC: 85, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 25, IP: 3, ADDR: 2, ACC: 85, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 26, IP: 3, ADDR: 1, ACC: 85, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 27, IP: 4, ADDR: 1, ACC: 76, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 28, IP: 5, ADDR: 1, ACC: 76, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 29, IP: 5, ADDR: 2, ACC: 76, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 30, IP: 6, ADDR: 2, ACC: 76, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 31, IP: 3, ADDR: 2, ACC: 76, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 32, IP: 3, ADDR: 1, ACC: 76, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 33, IP: 4, ADDR: 1, ACC: 68, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 34, IP: 5, ADDR: 1, ACC: 68, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 35, IP: 5, ADDR: 2, ACC: 68, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 36, IP: 6, ADDR: 2, ACC: 68, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 37, IP: 3, ADDR: 2, ACC: 68, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 38, IP: 3, ADDR: 1, ACC: 68, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 39, IP: 4, ADDR: 1, ACC: 32, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 40, IP: 5, ADDR: 1, ACC: 32, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 41, IP: 5, ADDR: 2, ACC: 32, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 42, IP: 6, ADDR: 2, ACC: 32, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 43, IP: 3, ADDR: 2, ACC: 32, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 44, IP: 3, ADDR: 1, ACC: 32, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 45, IP: 4, ADDR: 1, ACC: 67, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 46, IP: 5, ADDR: 1, ACC: 67, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 47, IP: 5, ADDR: 2, ACC: 67, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 48, IP: 6, ADDR: 2, ACC: 67, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 49, IP: 3, ADDR: 2, ACC: 67, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 50, IP: 3, ADDR: 1, ACC: 67, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 51, IP: 4, ADDR: 1, ACC: 65, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 52, IP: 5, ADDR: 1, ACC: 65, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 53, IP: 5, ADDR: 2, ACC: 65, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 54, IP: 6, ADDR: 2, ACC: 65, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 55, IP: 3, ADDR: 2, ACC: 65, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 56, IP: 3, ADDR: 1, ACC: 65, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 57, IP: 4, ADDR: 1, ACC: 84, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 58, IP: 5, ADDR: 1, ACC: 84, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 59, IP: 5, ADDR: 2, ACC: 84, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 60, IP: 6, ADDR: 2, ACC: 84, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 61, IP: 3, ADDR: 2, ACC: 84, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 62, IP: 3, ADDR: 1, ACC: 84, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 63, IP: 4, ADDR: 1, ACC: 32, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 64, IP: 5, ADDR: 1, ACC: 32, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 65, IP: 5, ADDR: 2, ACC: 32, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 66, IP: 6, ADDR: 2, ACC: 32, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 67, IP: 3, ADDR: 2, ACC: 32, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 68, IP: 3, ADDR: 1, ACC: 32, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 69, IP: 4, ADDR: 1, ACC: 84, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 70, IP: 5, ADDR: 1, ACC: 84, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 71, IP: 5, ADDR: 2, ACC: 84, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 72, IP: 6, ADDR: 2, ACC: 84, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 73, IP: 3, ADDR: 2, ACC: 84, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 74, IP: 3, ADDR: 1, ACC: 84, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 75, IP: 4, ADDR: 1, ACC: 72, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 76, IP: 5, ADDR: 1, ACC: 72, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 77, IP: 5, ADDR: 2, ACC: 72, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 78, IP: 6, ADDR: 2, ACC: 72, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 79, IP: 3, ADDR: 2, ACC: 72, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 80, IP: 3, ADDR: 1, ACC: 72, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 81, IP: 4, ADDR: 1, ACC: 73, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 82, IP: 5, ADDR: 1, ACC: 73, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 83, IP: 5, ADDR: 2, ACC: 73, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 84, IP: 6, ADDR: 2, ACC: 73, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 85, IP: 3, ADDR: 2, ACC: 73, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 86, IP: 3, ADDR: 1, ACC: 73, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 87, IP: 4, ADDR: 1, ACC: 83, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 88, IP: 5, ADDR: 1, ACC: 83, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 89, IP: 5, ADDR: 2, ACC: 83, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 90, IP: 6, ADDR: 2, ACC: 83, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 91, IP: 3, ADDR: 2, ACC: 83, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 92, IP: 3, ADDR: 1, ACC: 83, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 93, IP: 4, ADDR: 1, ACC: 33, Z: 0, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 94, IP: 5, ADDR: 1, ACC: 33, Z: 0, N: 0, SC: 0} 5 sw data 2
DEBUG:root:{TICK: 95, IP: 5, ADDR: 2, ACC: 33, Z: 0, N: 0, SC: 1} 5 sw data 2
DEBUG:root:{TICK: 96, IP: 6, ADDR: 2, ACC: 33, Z: 0, N: 0, SC: 0} 6 jmp absolute 3
DEBUG:root:{TICK: 97, IP: 3, ADDR: 2, ACC: 33, Z: 0, N: 0, SC: 0} 3 lw data 1
DEBUG:root:{TICK: 98, IP: 3, ADDR: 1, ACC: 33, Z: 0, N: 0, SC: 1} 3 lw data 1
DEBUG:root:{TICK: 99, IP: 4, ADDR: 1, ACC: 0, Z: 1, N: 0, SC: 0} 4 je absolute 7
DEBUG:root:{TICK: 100, IP: 7, ADDR: 1, ACC: 0, Z: 1, N: 0, SC: 0} 7 hlt data 0
INFO:root:output: SHOULD CAT THIS!
```

CI: Сделан по образцу преподавателя

## Апробация

Вариант: `asm | acc | neum | hw | tick | struct | stream | mem | prob1`

| ФИО            | алг.  | LoC | code байт | code инстр. | инстр. | такт. |
|----------------|-------|-----|-----------|-------------|--------|-------|
| Цыпандин Н. П. | hello | 59  | -         | 37          | 25     | 49    |
| Цыпандин Н. П. | cat   | 12  | -         | 11          | 67     | 101   |
| Цыпандин Н. П. | prob1 | 85  | -         | 43          | 4340   | 7813  | 
