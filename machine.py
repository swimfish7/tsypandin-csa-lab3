#!/usr/bin/python3
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=invalid-name                # сохраним традиционные наименования сигналов
# pylint: disable=consider-using-f-string     # избыточный синтаксис
# pylint: disable=missing-class-docstring
# pylint: disable=missing-module-docstring

import logging
import sys
from enum import Enum

from isa import Opcode, AddrMode, read_code, Term

MAX_WORD_VALUE = 9223372036854775807
MIN_WORD_VALUE = - 9223372036854775808


def handle_overflow(res) -> int:
    res = int(res)
    while res > MAX_WORD_VALUE:
        res = -MIN_WORD_VALUE + (res - MAX_WORD_VALUE)
    while res < MIN_WORD_VALUE:
        res = MAX_WORD_VALUE - (res + MIN_WORD_VALUE)
    return res


class AccLatchSignals(Enum):
    ALU = 0
    ARG = 1
    MEM = 2


class AluOperations(Enum):
    ADD = 0


opcode_to_alu_operation = {
    Opcode.ADD: AluOperations.ADD,
}


class ControlUnit:
    def __init__(self, memory, input_buffer):
        self.memory = memory
        self.input_buffer = input_buffer
        self.input_counter = 0
        self.output_buffer = []

        self._tick = 0

        self.acc = 0
        self.addr = 0
        self.ip = 0
        self.step_counter = 0

        self.z_flag = 0
        self.n_flag = 0

    def tick(self):
        self._tick += 1

    def get_tick(self):
        return self._tick

    def latch_acc(self, sel_acc, operation=AluOperations.ADD):
        res = 0
        if sel_acc == AccLatchSignals.ALU:
            res = self.alu_calculate(operation)
        elif sel_acc == AccLatchSignals.ARG:
            res = self.memory[self.ip]['term'].argument
        elif sel_acc == AccLatchSignals.MEM:
            if self.addr == 1:
                res = self.input_buffer[self.input_counter]
                self.input_counter += 1
            else:
                res = self.memory[self.addr]['term'].argument

        self.acc = handle_overflow(res)

        if self.acc == 0:
            self.z_flag = 1
        else:
            self.z_flag = 0

        if self.acc < 0:
            self.n_flag = 1
        else:
            self.n_flag = 0

    def latch_step_counter(self, sel_next):
        if sel_next:
            self.step_counter += 1
        else:
            self.step_counter = 0

    def latch_addr(self):
        res = self.memory[self.ip]['term'].argument
        self.addr = handle_overflow(res)

    def latch_ip(self, sel_next):
        if sel_next:
            res = self.ip + 1
        else:
            res = self.memory[self.ip]['term'].argument
        res = handle_overflow(res)
        self.ip = res

    def latch_mem(self):
        # should output
        if self.addr == 2:
            self.output_buffer.append(self.acc)
            return

        term = self.memory[self.addr]['term']
        self.memory[self.addr]['term'] = Term(term.line, term.operation, term.mode, self.acc)

    def alu_calculate(self, operation):
        operand = int(self.memory[self.addr]['term'].argument)
        operand = handle_overflow(operand)

        if operation == AluOperations.ADD:
            res = handle_overflow(self.acc + operand)
            return res
        return 0

    def decode_and_execute(self):
        instr = self.memory[self.ip]
        opcode = instr["opcode"]
        self.tick()

        if opcode is Opcode.HLT:
            raise StopIteration()

        if opcode in {Opcode.JMP, Opcode.JE, Opcode.JGE}:
            sel_next = False
            if opcode == Opcode.JE and self.z_flag == 0:
                sel_next = True
            elif opcode == Opcode.JGE and not (self.z_flag == 1 or self.n_flag == 0):
                sel_next = True

            self.latch_ip(sel_next)

        if opcode is Opcode.LW:
            if self.step_counter == 0:
                self.latch_addr()
                self.latch_step_counter(True)
            elif self.step_counter == 1:
                self.latch_acc(sel_acc=AccLatchSignals.MEM)
                self.latch_step_counter(False)
                self.latch_ip(True)

        if opcode in Opcode.SW:
            if self.step_counter == 0:
                self.latch_addr()
                self.latch_step_counter(True)
            elif self.step_counter == 1:
                self.latch_mem()
                self.latch_step_counter(False)
                self.latch_ip(True)

        if opcode is Opcode.ADD:
            if self.step_counter == 0:
                self.latch_addr()
                self.latch_step_counter(sel_next=True)
            elif self.step_counter == 1:
                self.latch_acc(AccLatchSignals.ALU, opcode_to_alu_operation[opcode])
                self.latch_step_counter(False)
                self.latch_ip(True)

    def __repr__(self):
        state = "{{TICK: {}, IP: {}, ADDR: {}, ACC: {}, Z: {}, N: {}, SC: {}}}".format(
            self._tick,
            self.ip,
            self.addr,
            self.acc,
            self.z_flag,
            self.n_flag,
            self.step_counter
        )

        instr = self.memory[self.ip]
        opcode = instr["opcode"]
        arg = instr["term"].argument
        mode = instr["term"].mode
        cell_num = instr["term"].line
        action = "{} {} {} {}".format(cell_num, opcode, mode, arg)

        return "{} {}".format(state, action)


def start_simulation(code, limit, input_buffer):
    control_unit = ControlUnit(code, input_buffer)
    instr_counter = 0
    logging.debug('%s', control_unit)

    try:
        while True:
            assert limit > instr_counter, "too long execution, increase limit!"
            control_unit.decode_and_execute()
            if control_unit.step_counter == 0:
                instr_counter += 1
            logging.debug('%s', control_unit)
    except StopIteration:
        pass

    output = ''
    for c in control_unit.output_buffer:
        if -128 <= c <= 127:
            output += chr(c)
        else:
            output += str(c)
    logging.info('output: %s', output)
    return output, instr_counter, control_unit.get_tick()


def main(args):
    code_file, input_file, memory_amount = args
    assert int(memory_amount) > 0, 'Memory argument should be positive number'
    memory_amount = int(memory_amount)

    # process user input and prepare buffer
    input_tokens = []
    with open(input_file, encoding="utf-8") as file:
        input_text = file.read()
        for char in input_text:
            input_tokens.append(ord(char))
    input_tokens.append(0)

    memory = read_code(code_file)
    cell_counter = len(memory)

    # Allocate memory to 1024 machine words for single program
    # memory_per_program = 1024
    assert_msg = 'Not enough memory argument passed for program execution!'
    assert cell_counter < memory_amount, assert_msg
    while cell_counter < memory_amount:
        memory.append({'opcode': Opcode.DATA,
                       'term': Term(cell_counter, 'data', AddrMode.DATA, '0')})
        cell_counter += 1

    max_steps = 10_000
    output, instr_counter, ticks = start_simulation(memory, max_steps, input_tokens)

    print("Output:")
    print(output)
    print("instr_counter:", instr_counter, "ticks:", ticks)
    return output


if __name__ == '__main__':
    msg = 'Wrong number of arguments: machine.py <code_file> <input_file> <memory_amount>'
    assert len(sys.argv) == 4, msg
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
