#!/usr/bin/python3
# pylint: disable=missing-class-docstring
# pylint: disable=missing-module-docstring

import unittest

import isa
import translator


class TranslatorTest(unittest.TestCase):
    def simple_test(self, input_file, output, correct):
        translator.main([input_file, output])

        result_code = isa.read_code(output)
        correct_code = isa.read_code(correct)

        self.assertEqual(result_code, correct_code)

    def test_hello(self):
        self.simple_test('tests/hello', 'tests/hello_res', 'tests/hello_res_expected')

    def test_cat(self):
        self.simple_test('tests/cat', 'tests/cat_res', 'tests/cat_res_expected')

    def test_prob1(self):
        self.simple_test('tests/prob1', 'tests/prob1_res', 'tests/prob1_res_expected')
